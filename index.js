// console.log('Hello World');

// Parameters and Arguments

const printName = (name) => { // paramater
	console.log(`My name is ${name}.`);
};

let ngalan = 'Chris';
let nawa = 'Shula';
printName(ngalan); // argument
printName(nawa);

// const printNames = () => { // paramater
// 	let params = 'Mary';
// 	console.log(params);
// }

// printNames('Hello');

const checkDivisibilityBy8 = (num) => {
	let rem = num % 8;
	console.log(`The remainder of ${num} divided by 8 is ${rem}.`);

	let isDivisibleBy8 = rem === 0;
	console.log(`Is ${num} divisible by 8?`);
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(17);

//

const argumentFunctionOne = () => {
	console.log('Hello');
};

const argumentFunctionTwo = () => {
	console.log('Hi');
};

const invokeFunction = argFunction => {
	console.log(argFunction);
};

invokeFunction(argumentFunctionTwo);
invokeFunction(argumentFunctionOne);

// multiple parameters

const createFullName = (firstName = 'no firstname', middleName, lastName) => {
	console.log(`First Name: ${firstName}`);
	console.log(`Middle Name: ${middleName}`);
	console.log(`Last Name: ${lastName}`);
};

let first;
let middle = 'Two';
let last = 'Three';
let yow = 'Chris';
createFullName(first, middle, last, yow);